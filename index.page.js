// @ts-check
import { html } from '@small-web/kitten'

export default function (_request, _response) {
  return html`
    <page water>
    <markdown>
      # Small Web Place

      This is currently a placeholder for testing deployments with [Kitten](https://codeberg.org/kitten/app) and [Domain](https://codeberg.org/domain/app).
    </markdown>
  `
}
